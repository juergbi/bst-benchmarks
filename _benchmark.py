#!/usr/bin/env python3

from collections import defaultdict
from contextlib import suppress
import json
import logging
import os
import shutil
import subprocess
import sys
from typing import Callable, NamedTuple, Optional

import click


LOGGER = logging.getLogger()


Action = NamedTuple(
    "Action",
    [
        ("name", str),
        ("command", str),
        ("cleanup", Optional[Callable[[], None]]),
    ],
)


def time_buildstream(command, workdir):
    proc = subprocess.Popen(
        [
            "/usr/bin/time",
            "--format",
            "-- %e %M",
            "bash",
            "-c",
            "{} >/dev/null 2>/dev/null".format(" ".join(command)),
        ],
        cwd=workdir,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.PIPE,
        universal_newlines=True,
    )

    assert not proc.wait()

    result = {}

    for line in proc.stderr.readlines():
        if line.startswith("--"):
            seconds, max_memory = line.strip("-").strip().split(" ")
            return {
                "max_memory": int(max_memory),
                "clock_time": float(seconds),
            }

    raise Exception("Result unable to be parsed successfully", result)


@click.command()
@click.option(
    "--project-path",
    required=True,
    type=click.Path(exists=True, dir_okay=True),
)
@click.option("--measurements", type=int, required=True)
@click.option("--builders", type=int, multiple=True)
@click.argument("files", nargs=-1, required=True)
def benchmark(files, measurements, builders, project_path):
    logging.basicConfig(level=logging.DEBUG, format="%(message)s")

    def clean_buildstream_cache():
        with suppress(FileNotFoundError):
            shutil.rmtree(
                os.path.join(os.environ["XDG_CACHE_HOME"], "buildstream")
            )

    def clean_project_cache():
        with suppress(FileNotFoundError):
            shutil.rmtree(os.path.join(project_path, ".bst"))

    def clean_all():
        clean_buildstream_cache()
        clean_project_cache()

    results = defaultdict(list)

    steps = [
        Action(
            name="bst show without cache",
            cleanup=clean_all,
            command=["bst", "show", *files],
        ),
        Action(
            name="bst show with cache",
            cleanup=clean_buildstream_cache,
            command=["bst", "show", *files],
        ),
    ]
    steps.extend(
        Action(
            name="bst build - {} builders".format(n_builders),
            cleanup=clean_buildstream_cache,
            command=["bst", "--builders", n_builders, "build", *files],
        )
        for n_builders in builders
    )
    steps.append(
        Action(
            name="bst show everything cached",
            cleanup=None,
            command=["bst", "show", *files],
        )
    )

    for index, step in enumerate(steps):
        LOGGER.info("[%d/%d] command: %s", index + 1, len(steps), step.name)

        for iteration in range(measurements + 1):
            if iteration == 0:
                message = "warming up"
            else:
                message = "benchmarking"

            LOGGER.info(
                "[%d/%d][%d/%d] %s",
                index + 1,
                len(steps),
                iteration + 1,
                measurements,
                message,
            )

            if step.cleanup is not None:
                step.cleanup()

            data = time_buildstream(step.command, project_path)

            if iteration != 0:
                results[step.name].append(data)

    json.dump(results, sys.stdout)


if __name__ == "__main__":
    benchmark()
