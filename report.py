import pandas as pd
import json
from tabulate import tabulate

import click


@click.command()
@click.argument("report_file", type=click.File("r"))
def report(report_file):
    data = json.load(report_file)

    csv = [
        [branch, interpreter, step, result["clock_time"], result["max_memory"]]
        for branch, values in data.items()
        for interpreter, steps in values.items()
        for step, results in steps.items()
        for result in results
    ]

    labels = ["branch", "interpreter", "step", "clock_time", "max_memory (MB)"]

    df = pd.DataFrame\
            .from_records(csv, columns=labels)\
            .groupby(["step", "branch", "interpreter"])\
            .median()\
            .reset_index()

    df["max_memory (MB)"] /= 1024
    df["max_memory (MB)"] = df["max_memory (MB)"].round()

    values = df.values.tolist()

    last_step = None
    for value in values:
        if value[0] == last_step:
            value[0] = ""
        else:
            last_step = value[0]

    print("Report is")
    print("-" * 80)
    print(tabulate(values, headers=df.columns.values, tablefmt="pipe"))
    print("-" * 80)


if __name__ == "__main__":
    report()
