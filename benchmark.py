#!/usr/bin/env python3

from collections import defaultdict
from contextlib import ExitStack
import itertools
import json
import os
import re
import subprocess

import click
from tqdm import tqdm


SUPPORTED_PYTHON_VERSIONS = ["py35", "py36", "py37"]
BENCHMARKED_PROJECT = "https://gitlab.com/jennis/debian-stretch-bst.git"

ROOT_DIR = os.path.dirname(__file__)
DEFAULT_WORKDIR = os.path.join(os.getcwd(), ".cache")
TOX_WORKDIR = os.path.join(DEFAULT_WORKDIR, "tox")

re_command_running = re.compile(
    r"\[\d+\/(?P<total_steps>\d+)\]\s(?P<message>.*)"
)
re_step_running = re.compile(r"\[\d+\/\d+\]\[\d+\/\d+\]\s(?P<message>.*)")


def get_project(path):
    if os.path.exists(path):
        subprocess.check_output(["git", "fetch"], cwd=path)
        subprocess.check_output(
            ["git", "reset", "--hard", "origin/master"], cwd=path
        )
    else:
        subprocess.check_output(["git", "clone", BENCHMARKED_PROJECT, path])


def print_proc_progress(stderr, measurements):
    with tqdm(total=1, leave=False) as progress_bar, ExitStack() as stack:
        for line in stderr:
            line = line.strip()

            match = re_command_running.match(line)

            if match is not None:
                stack.pop_all().close()
                subprogress_bar = tqdm(total=measurements + 1, leave=False)
                stack.enter_context(subprogress_bar)
                progress_bar.total = int(match.group("total_steps"))
                progress_bar.set_description(match.group("message"))
                progress_bar.update(1)

            else:
                match = re_step_running.match(line)

                if match is not None:
                    subprogress_bar.update(1)
                    subprogress_bar.set_description(match.group("message"))
                else:
                    tqdm.write(line)


def benchmark_commit(
    commit,
    python_version,
    measurements,
    builders,
    project_path,
    cache_directory,
    files,
):
    environ = os.environ.copy()

    tox_venv = os.path.join(TOX_WORKDIR, python_version)

    environ.update({"BST_COMMIT": commit, "XDG_CACHE_HOME": cache_directory})

    subprocess.check_call(
        [
            "tox",
            "-qq",
            "--notest",
            "--recreate",
            "--workdir",
            TOX_WORKDIR,
            "-e",
            python_version,
        ],
        env=environ,
    )

    environ.update(
        {
            "PATH": "{}:{}".format(
                os.path.join(tox_venv, "bin"), os.environ["PATH"]
            )
        }
    )

    proc = subprocess.Popen(
        [
            "python",
            os.path.join(ROOT_DIR, "_benchmark.py"),
            "--measurements",
            str(measurements),
            "--project-path",
            project_path,
            *itertools.chain.from_iterable(
                zip(itertools.repeat("--builders"), map(str, builders))
            ),
            *files,
        ],
        env=environ,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        universal_newlines=True,
    )

    print_proc_progress(proc.stderr, measurements)

    assert proc.wait() == 0
    return json.load(proc.stdout)


def run_benchmark(
    commits,
    python_versions,
    measurements,
    builders,
    project_path,
    cache_directory,
    files,
):
    tqdm_python_versions = tqdm(python_versions)
    results = defaultdict(dict)

    for python_version in tqdm_python_versions:
        tqdm_python_versions.set_description(f"Testing for {python_version}")

        tqdm_commits = tqdm(commits, leave=False)
        for commit in tqdm_commits:
            tqdm_commits.set_description(f"Benchmarking commit '{commit}'")

            result = benchmark_commit(
                commit,
                python_version,
                measurements,
                builders,
                project_path,
                cache_directory,
                files,
            )

            results[commit][python_version] = result

    return results


@click.command()
@click.option(
    "--python-version",
    "python_versions",
    type=click.Choice(SUPPORTED_PYTHON_VERSIONS),
    multiple=True,
    default=SUPPORTED_PYTHON_VERSIONS,
)
@click.option(
    "--project-path",
    type=click.Path(dir_okay=True),
    default=os.path.join(DEFAULT_WORKDIR, "benchmarked-project"),
)
@click.option(
    "--cache-path",
    type=click.Path(dir_okay=True),
    default=os.path.join(DEFAULT_WORKDIR, "cache"),
)
@click.option("--measurements", type=int, default=5)
@click.option(
    "--report-path",
    type=click.Path(file_okay=True, writable=True),
    default=os.path.join(os.getcwd(), "report.json"),
)
@click.option("--builders", type=int, multiple=True)
@click.argument("reference")
@click.argument("candidate")
@click.argument("files", nargs=-1)
def compare(
    builders,
    reference,
    candidate,
    project_path,
    measurements,
    python_versions,
    cache_path,
    files,
    report_path,
):
    if not files:
        files = ["essential-stack.bst"]

    get_project(project_path)
    results = run_benchmark(
        [reference, candidate],
        python_versions,
        measurements,
        builders,
        project_path,
        cache_path,
        files,
    )

    with open(report_path, "w") as fp:
        json.dump(results, fp)


if __name__ == "__main__":
    compare()
